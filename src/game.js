import { Application, Assets} from "pixi.js";
import { GAME_HEIGHT,GAME_WIDTH } from "./gameConstant";
import { GameScene } from "./scene/gameScene";
import {manifest} from "../config.json";
export default class Game {
    constructor() {
        this.app = new Application({
          view: document.getElementById("main"),
          resolution: window.devicePixelRatio | 1,
          antialias: true,
          backgroundColor: 0x23232f,
          width: GAME_WIDTH,
          height: GAME_HEIGHT,
        });
        Assets.init({ manifest: manifest });
      }
    
      play() {
        this.gameScene = new GameScene();
        this.app.stage.addChild(this.gameScene);
      }
}