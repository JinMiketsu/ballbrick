import { Container } from "pixi.js";
import { Ball } from "./ball";

export class BallManager extends Container{
    constructor(){
        super();

        this.initListBall();

    }
    initListBall(){
        this.listBalls = [];
        this.ball = new Ball();
        this.addChild(this.ball);
    }
    onShootStart(angle){
        this.ball.onMoving = true;
        this.ball.vx=Math.cos(angle) * this.ball.speed;
        this.ball.vy=Math.sin(angle) * this.ball.speed;
        this.ShootingQuasar();
    }
    ShootingQuasar(){
        if (this.ball.onMoving = true){
            this.ball.x += this.ball.vx;
            this.ball.y += this.ball.vy;
        }
    }
}