import { ObjectSprite } from "../object_sprite/sprite";
import { Container } from "pixi.js";
import { GAME_HEIGHT,GAME_WIDTH } from "../../gameConstant";

export default BallEvent({
    Stop:"ball:stop",
    Colliding:"ball:colliding",
})
export class Ball extends ObjectSprite(){
    constructor(radius=8 ,speed=10){
        super();
        this.radius = radius;
        this.speed = speed;
        this.onMoving= false;
       
        this.x = GAME_WIDTH/2;
        this.y = GAME_HEIGHT-80;

    }

 
  update(dt){
    super.update(dt);
  }
  setPosition(x, y){
    super.setPosition(x, y);
    this.preX = this.x;
    this.preY = this.y;
  }


}
