import { Graphics } from "pixi.js";
import { GameAnchor } from "../constant";
import { calcAngle, calcVector } from "../utils/utils";

export default class PredictLine extends Graphics {
    constructor() {
        super();
    }

    setPosition(x, y) {
        this.mx = x;
        this.my = y;
    }

    draw(e) {
        let x = 10 * e.x - 9 * x1;
        let y = 10 * e.y - 9 * y1;

        let vector = calcVector(x1, y1, x, y);
        let angle = calcAngle(1, 0, vector.x, vector.y);
        let degrees = (angle * 180) / Math.PI;
        this.angleShoot = angle;
        
        this.clear();
        this.lineStyle(1, 0xffffff, 1);
        this.moveTo(x1, y1);
        this.lineTo(x, y);
    }
}

