import { Container, Sprite, Texture } from "pixi.js";

export class gameScene extends Container{
    constructor(){
        super();
        create();
    }
    create(){
        this.texture = Texture.from('images/screen.png')
        this.playmat = Sprite(this.texture);
        this.addChild(this.playmat) ;
    }
}