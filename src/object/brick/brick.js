import { Graphics, Texture } from "pixi.js";
import { calDistance, calVector } from "../../utils/utils";
import { ObjectSprite } from "../object_sprite/sprite";


export class Brick extends ObjectSprite{
    constructor(x,y,w,h){
        super();
    
    this.brick = new Graphics;
    this.brick.beginFill(0xf51d05);
    this.brick.draw(x,y,w,h);
    this.brick.endFill();
    this.point=[];
    this.point[0]=[x,y];
    this.point[1]=[x+w,y];
    this.point[2]=[x,y+h];
    this.point[3]=[x+w,y+h];

    this.edge = calVector(
        this.point[0][0],
        this.point[0][1],
        this.point[1][0],
        this.point[1][1]
    );
    this.length = calDistance(this.edge.x ,this.edge.y);
    this.addChild(this.brick);
    }

}