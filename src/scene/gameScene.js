import { Assets, Container, Ticker,Rectangle } from "pixi.js";
import { getSpriteFromCache } from "../utils/util";
import { GAME_HEIGHT,GAME_WIDTH } from "../gameConstant";
import { BallManager } from "../object/ball/ball_manager";
import PredictLine from "../object/predict_line/predict_line";
import { Rectangle } from "@pixi/particle-emitter/lib/behaviors/shapes";
import { Brick } from "../object/brick/brick";
export class GameScene extends Container {
  constructor() {
    super();
    Assets.loadBundle("game-screen").then(this.setup.bind(this));
    this.gameContainer = new Container();
    this.addChild(this.gameContainer);
    this.setup();
  }

  setup() {
    // setup background
    const background = getSpriteFromCache("background");
    background.height = GAME_HEIGHT;
    background.width = GAME_WIDTH;
    this.gameContainer.addChild(background);

    // set up vat the
    initBall();
    initPredictionLine();
    initBrick();
    initEvent();

    Ticker.shared.add(this.loop.bind(this));
  }
  initBall(){
    this.ball = new BallManager();
    this.addChild(this.ball);
  }
  initPredictionLine(){
    this.predictionLine = new PredictLine();
    this.addChild(this.predictionLine);
    this.predictionLine.setPosition(
      GAME_WIDTH/2,
      GAME_HEIGHT-80
    )
    this.interactive = true;
    this.hitArea = new Rectangle(0,70,GAME_WIDTH,GAME_HEIGHT);
  }
  initBrick(){
    this.brick = new Brick(GAME_WIDTH/2,GAME_HEIGHT,40,40);
    this.addChild(this.brick);
  }
  initEvent(){
    this.on("pointermove",this.onMouseDragging,this);
    this.on("pointerdown",this.onMouseDragEnd,this)
  }
  loop(delta) {
    this.ball.update(dt);
    this.test
  }
}