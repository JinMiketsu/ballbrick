import * as PIXI from "pixi.js"
import { Assets, Sprite } from "pixi.js";

export function randomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

export function getSprite(name){
   return new Sprite(Assets.cache.get(name));
}

export function calDistance(x1, y1, x2 = undefined, y2 = undefined) {
    if(x2 && y2) {
        return Math.sqrt((x2-x1)**2 + (y2-y1)**2);
    }
    else{
        return Math.sqrt(x1**2 + y1**2);
    }
}
export function calVector(x1, y1, x2, y2){
    return {
        x: x2-x1,
        y: y2-y1
    }
}

export function calReflecVector(x1,y1,x2,y2z){
    return {
        x: x1-x2,
        y: y2-y1
    }
}