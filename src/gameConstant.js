export const GAME_WIDTH = 360;
export const GAME_HEIGHT = 600;
export const GAME_TOP_Y= 80;
export const GAME_BOT_Y=520;
export const ball_radius= 4;
export const GamePanel = Object.freeze({
    topLeft:{
        x:0,
        y:0,   
    },
    topRight:{
        x:GAME_WIDTH,
        y:0,
    },
    downLeft:{
        x:0,
        y:GAME_HEIGHT,
    },
    downRight:{
        x:GAME_WIDTH,
        y:GAME_HEIGHT,
    }
})
