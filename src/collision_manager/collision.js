import {GameContainer} from "pixi.js"
export default ColliderEvent({
    Stop:":bound_circle:stop",
    Colliding:"bound_circle:colliding",
})


export class CollisionDetector {
    static init(){
        this.addTag1 = [];
        this.addTag2 = [];
        this.addTag3 = [];
    }
    static addTag1(collider){
        this.tag1.push(collider);
    }
    static addTag2(collider){
        this.tag2.push(collider);
    }
    static addTag3(collider){
        this.tag3.push(collider);
    }
    static update(){
        this.tag1.forEach((collider1) => {
            this.tag2.forEach((collider2) => {
                if(collider1.enable && collider2.enable){
                    dmgCounter += dmgCounter + ballDmg;
                }
            });
        });
    }
    
}
   